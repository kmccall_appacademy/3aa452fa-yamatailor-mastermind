class Code
  attr_reader :pegs

  PEGS = { "r" => "red", "g" => "green", "b" => "blue",
           "y" => "yellow", "o" => "orange", "p" => "purple" }.freeze

  def self.parse(input)
    input = input.downcase.chars

    input.each do |_ch|
      if input.all? { |x| PEGS.include?(x) }
        return Code.new(input)
      else
        raise ArgumentError
      end
    end
  end

  def initialize(pegs)
    @pegs = pegs
  end

  def self.random
    pegs = PEGS.keys.shuffle[0..3]
    Code.new(pegs)
  end

  def [](pegs)
    @pegs[pegs]
  end

  def exact_matches(given)
    @pegs.each_with_index.inject(0) do |count, (color, i)|
      given.pegs[i] == color ? count += 1 : count
    end
  end

  def near_matches(given)
    pegs = given.pegs
    pegs.each_with_index.inject([]) do |arr, (el, i)|
      return 0 if pegs[i] == @pegs[i]
      @pegs.include?(el) && !arr.include?(el) ? arr << el : arr
    end.count
  end

  def ==(given)
    return false if given.class != Code
    return true if self.pegs == given.pegs
  end
end

class Game
  attr_reader :secret_code

  def initialize(secret_code = Code.random)
    @secret_code = secret_code
  end

  def get_guess
    p "What is your guess?"
    answer = gets.chomp
    Code.parse(answer)
  end

  def display_matches(given)
    exact_match = @secret_code.exact_matches(given)
    near_match = @secret_code.near_matches(given)

    p "Your exact match was #{exact_match} and near_match was #{near_match}"
  end
end
